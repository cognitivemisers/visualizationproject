/*
 * History HeatMap          -- Object constructor function
 * @param _parentElement 	-- the HTML element in which to draw the heatmap visualization
 * @param _data				-- multiple datasets
 */
HeatMap = function(_parentElement, _data){
    this.parentElement = _parentElement;
    this.data = _data;
    this.deaths = _data[0];
    this.new_hiv_infections = _data[1];
    this.people_living_with_hiv = _data[2];

    this.displayData = []; // see data wrangling
    this.formattedData = {};
    this.yearSlider = null;
    this.selectedYear = "Current";
    this.selectedRegion = "Asia and Pacific";
    this.selectedGroup = "All ages";
    this.selectedIndicator = "Deaths";
    this.selectedSort = "sortinit_col_row";

    // DEBUG RAW DATA
    //console.log(this.data);

};

/*
 * Initialize DOM - add HTML from historic.html
 * Bind event listeners and call initVis
 */
HeatMap.prototype.initDOM = function() {

    var vis = this;

    $.get("includes/historic.html", function(data) {
        $("#historic-vis").append(data);

        //Toggle initial button
        $("#h-map-primary-indicator").addClass('active');

        vis.bindEventListeners();
        vis.initVis();
    });

};

/*
 * Register Event Listeners
 */
HeatMap.prototype.bindEventListeners = function(){

    var vis = this;

    // Capture change event of regions radio buttons
    $('input[name="h-regionGroup"]').change( function() {
        vis.selectedRegion = $(this).val();
        vis.wrangleData();
    });

    // Capture change event of age group radio buttons
    $('input[name="h-ageGroup"]').change( function() {
        vis.selectedGroup = $(this).val();
        vis.wrangleData();
    });

    // Capture click event of data buttons
    $("#h-map-btn-indicator .btn").click(function(){
        $(this).button('toggle');
        $(this).siblings('.btn').removeClass('active');
        vis.selectedIndicator= $(this).attr("value");
        vis.wrangleData();
    });

    // Capture change event of sort dropdown selection
    $("#sort-heatmap").on("change", function() {
        vis.selectedSort = $(this).val();
        vis.changeOrder();
    });

};

/*
 * Initialize visualization (static content, e.g. SVG area or axes)
 */
HeatMap.prototype.initVis = function(){

    var vis = this;
    vis.data = vis.deaths;

    // Filter the dataset based on user's selection (Region and Agegroup)
    vis.displayData = vis.data.filter(function(d) {return (d.Region == vis.selectedRegion && d.Subgroup == vis.selectedGroup)});

    // Prepare the data structure for visual
    vis.formattedData.columns = vis.displayData.map(function(d) { return +formatDate(d.Year); }).filter(function(item, i, c){ return i == c.indexOf(item); }).sort();
    vis.formattedData.index = vis.displayData.map(function(d) { return d.Country; }).filter(function(item, i, c){ return i == c.indexOf(item); }).sort();

    var cntry = [];

    // Time series data has gaps, loop thru and fill the gap with null value
    for (row = 0; row < vis.formattedData.index.length; row++){
        var yrs = [];
        for (col = 0; col < vis.formattedData.columns.length; col++){
            var missingValue = true;
            vis.displayData.forEach(function(d){
                if (d.Country == vis.formattedData.index[row] && +formatDate(d.Year) == vis.formattedData.columns[col]) {
                    missingValue = !missingValue;
                    yrs.push(d.Value);
                }
            });

            // Create null value for missing data in the timeseries
            if (missingValue) {
                yrs.push(null);
            }

        }
        cntry.push(yrs);
    }

    vis.formattedData.data = cntry;

    // Set margin for SVG
    vis.margin = { top: 30, right: 20, bottom: 15, left: 10 };

    // Set the size for the tile
    vis.cellSize = 25;

    // Set position of the HeatMap
    vis.viewerPosTop = 100;
    vis.viewerPosLeft = 100;

    // Set row / column labels
    vis.rowLabel = vis.formattedData.index;
    vis.colLabel = vis.formattedData.columns;

    // Set the row / column length
    vis.col_number = vis.colLabel.length;
    vis.row_number = vis.rowLabel.length;

    // Set dyanmically heatmap grid size based number of tiles
    vis.width = vis.cellSize * vis.col_number + 80; //- vis.margin.left - vis.margin.right;
    vis.height = vis.cellSize * vis.row_number + 350; //- vis.margin.top - vis.margin.bottom;

    // Set legend width
    vis.legendElementWidth = vis.cellSize * 2.5;

    // Number of category for color coding
    vis.classesNumber = 6;

    // Set the color palette
    vis.paletteName = "Paired";
    vis.colors = colorbrewer[vis.paletteName][vis.classesNumber];

    // Zoom the svg geometrically
    function zoom() {
        vis.svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
    }

    // define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
    vis.zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on("zoom", zoom);

    // Set the color scale
    vis.colorScale = d3.scale.quantile()
        .domain([0, d3.max(vis.displayData, function(d) { return d.Value; })])
        .range(vis.colors);


    // Create the SVG element
    vis.svg = d3.select("#h-heatmap").append("svg")
        .attr("width", vis.width + vis.margin.left + vis.margin.right)
        .attr("height", vis.height + vis.margin.top + vis.margin.bottom)
        //.call(vis.zoomListener)
        .append("g")
        .attr("transform", "translate(" + vis.viewerPosLeft + "," + vis.viewerPosTop + ")");

    // No data tile
    vis.svg.append('defs')
        .append('pattern')
        .attr('id', 'diagonalHatch')
        .attr('patternUnits', 'userSpaceOnUse')
        .attr('width', 4)
        .attr('height', 4)
        .append('path')
        .attr('d', 'M-1,1 l2,-2 M0,4 l4,-4 M3,5 l2,-2')
        .attr('stroke', '#000000')
        .attr('stroke-width', 1);

    // Create the tooltip function
    vis.tooltip = {
        element: null,
        init: function() { this.element = d3.select("body").append("div").attr("class", "tooltip").style("opacity", 0); },
        show: function(t) { this.element.html(t).transition().duration(200).style("left", d3.event.pageX + 20 + "px").style("top", d3.event.pageY - 20 + "px").style("opacity", .9); },
        move: function() { this.element.transition().duration(30).ease("linear").style("left", d3.event.pageX + 20 + "px").style("top", d3.event.pageY - 20 + "px").style("opacity", .9); },
        hide: function() { this.element.transition().duration(500).style("opacity", 0) }};

    // Initialize the tooltip functions
    vis.tooltip.init();

    // Initialize sort orders
    vis.rowSortOrder = false;
    vis.colSortOrder = false;

    // Add row labels
    var rowLabels = vis.svg.append("g")
        .attr("class", "rowLabels")
        .selectAll(".rowLabel")
        .data(vis.formattedData.index)
        .enter().append("text")
        .text(function(d) { return d; })
        .attr("x", 0)
        .attr("y", function(d, i) { return (i * vis.cellSize); })
        .style("text-anchor", "end")
        .attr("transform", function(d, i) { return "translate(-3," + vis.cellSize / 1.5 + ") rotate(-45, 0, " + (i * vis.cellSize) + ")"; })
        .attr("class", "rowLabel mono")
        .attr("id", function(d, i) { return "rowLabel_" + i; })
        .on('mouseover', function(d, i) { d3.select('#rowLabel_' + i).classed("hover", true); })
        .on('mouseout', function(d, i) { d3.select('#rowLabel_' + i).classed("hover", false); })
        .on("click", function(d, i) {
            vis.rowSortOrder = !vis.rowSortOrder;
            vis.sortByLabels("r", i, vis.rowSortOrder);
            d3.select("#order").property("selectedIndex", 0);
        });

    // Add column labels
    var colLabels = vis.svg.append("g")
        .attr("class", "colLabels")
        .selectAll(".colLabel")
        .data(vis.formattedData.columns)
        .enter().append("text")
        .text(function(d) { return d; })
        .attr("x", 0)
        .attr("y", function(d, i) { return (i * vis.cellSize); })
        .style("text-anchor", "left")
        .attr("transform", function(d, i) { return "translate(" + vis.cellSize / 2 + ", -3) rotate(-90) rotate(45, 0, " + (i * vis.cellSize) + ")"; })
        .attr("class", "colLabel mono")
        .attr("id", function(d, i) { return "colLabel_" + i; })
        .on('mouseover', function(d, i) { d3.select('#colLabel_' + i).classed("hover", true); })
        .on('mouseout', function(d, i) { d3.select('#colLabel_' + i).classed("hover", false); })
        .on("click", function(d, i) {
            vis.colSortOrder = !vis.colSortOrder;
            vis.sortByLabels("c", i, vis.colSortOrder);
            d3.select("#sort-heatmap").property("selectedIndex", 0);
        });

    // Add a row of tiles
    var row = vis.svg.selectAll(".row")
        .data(vis.formattedData.data)
        .enter().append("g")
        .attr("id", function(d) { return d.idx; })
        .attr("class", "row");

    var j = 0;
    var heatMap = row.selectAll(".cell")
        .data(function(d) {
            j++;
            return d;
        })
        .enter().append("svg:rect")
        .attr("x", function(d, i) { return i * vis.cellSize; })
        .attr("y", function(d, i, j) { return j * vis.cellSize; })
        .attr("rx", 4)
        .attr("ry", 4)
        .attr("class", function(d, i, j) { return "cell bordered cr" + j + " cc" + i; })
        .attr("row", function(d, i, j) { return j; })
        .attr("col", function(d, i, j) { return i; })
        .attr("width", vis.cellSize)
        .attr("height", vis.cellSize)
        //.style("fill", function(d) { if (d != null) return vis.colorScale(d); else return "url(#diagonalHatch)"; })
        .on('mouseover', function(d, i, j) {
            d3.select('#colLabel_' + i).classed("hover", true);
            d3.select('#rowLabel_' + j).classed("hover", true);
            d3.select(this).classed("cell-hover",true);
            d3.select(this).transition().duration(300).style("opacity", 0.8);
            if (d != null) {
                vis.tooltip.show("<b>" + numFormat(d) + " </b>" + vis.selectedIndicator);
            } else {
                vis.tooltip.show("<b>No Data Available</b>");
            }
        })
        .on('mouseout', function(d, i, j) {
            d3.select('#colLabel_' + i).classed("hover", false);
            d3.select('#rowLabel_' + j).classed("hover", false);
            d3.select(this).classed("cell-hover",false);
            d3.select(this).transition().duration(300).style("opacity", 1);
            vis.tooltip.hide();
        })
        .on("mousemove", function(d, i) {
            vis.tooltip.move();
        })

    // Add transition to the tiles
    heatMap.transition().duration(1000).style("fill", vis.colorScale(0));

    heatMap.transition().duration(1000)
        .style("fill", function(d) { if (d != null) return vis.colorScale(d); else return "url(#diagonalHatch)"; });

    // Add legend
    var legend = vis.svg.append("g")
        .attr("class", "legend")
        .attr("transform", "translate(" + vis.width/5 + ",-200)")
        .selectAll(".legendElement")
        .data([0].concat(vis.colorScale.quantiles()), function(d) { return d; })
        .enter().append("g")
        .attr("class", "legendElement");

    legend.append("svg:rect")
        .attr("x", function(d, i) { return vis.legendElementWidth * i; })
        .attr("y", vis.viewerPosTop)
        .attr("class", "cellLegend bordered")
        .attr("width", vis.legendElementWidth)
        .attr("height", vis.cellSize / 2)
        .style("fill", function(d, i) { return vis.colors[i]; });

    legend.append("text")
        .attr("class", "mono legendText")
        .text(function(d) { return formatAbbreviation(Math.round(d)); })
        .attr("x", function(d, i) { return vis.legendElementWidth * i; })
        .attr("y", vis.viewerPosTop + vis.cellSize);

};

/**
 * This function will format the numbers in to numberic abbreviation
 * for Ex: formatAbbreviation(1000) will return "1K"
 * formatAbbreviation(1000000) -> "1M"
 * @param x : Numeric Value
 * @returns {*} : Abbreviated numeric value
 */
function formatAbbreviation(x) {

    var formatSi = d3.format(".3s");
    var s = formatSi(x);
    switch (s[s.length - 1]) {
        case "G":
            return s.slice(0, -1) + "B";
    }
    return s;

}

/**
 * This function will sort the heatmap tiles by colomn / row labels
 * @param rORc  : Indicates whether row or column label clicked
 * @param i     : ith Label row / column element
 * @param sortOrder : sort order (ascending / descending)
 */
HeatMap.prototype.sortByLabels = function(rORc, i, sortOrder){

    var vis = this;

    var t = vis.svg.transition().duration(2000);
    var values = [];
    var sorted;

    d3.selectAll(".c" + rORc + i)
        .filter(function(d) {
            if (d != null) values.push(d);
            else values.push(-999); // to handle NaN
        });

    //console.log(values);

    if (rORc == "r") { // sort on cols
        sorted = d3.range(vis.col_number).sort(function(a, b) {
            if (sortOrder) {
                return values[b] - values[a];
            } else {
                return values[a] - values[b];
            }
        });
        t.selectAll(".cell")
            .attr("x", function(d) {
                var col = parseInt(d3.select(this).attr("col"));
                return sorted.indexOf(col) * vis.cellSize;
            });
        t.selectAll(".colLabel")
            .attr("y", function(d, i) {
                return sorted.indexOf(i) * vis.cellSize;
            })
            .attr("transform", function(d, i) {
                return "translate(" + vis.cellSize / 2 + ", -3) rotate(-90) rotate(45, 0, " + (sorted.indexOf(i) * vis.cellSize) + ")";
            });
    } else { // sort on rows
        sorted = d3.range(vis.row_number).sort(function(a, b) {
            if (sortOrder) {
                return values[b] - values[a];
            } else {
                return values[a] - values[b];
            }
        });
        t.selectAll(".cell")
            .attr("y", function(d) {
                var row = parseInt(d3.select(this).attr("row"));
                return sorted.indexOf(row) * vis.cellSize;
            });
        t.selectAll(".rowLabel")
            .attr("y", function(d, i) {
                return sorted.indexOf(i) * vis.cellSize;
            })
            .attr("transform", function(d, i) {
                return "translate(-3," + vis.cellSize / 1.5 + ") rotate(-45, 0, " + (sorted.indexOf(i) * vis.cellSize) + ")";
            });
    }
};

/**
 * This function sort the Heatmap tiles based on row order, column order or both (ascending only)
 */
HeatMap.prototype.changeOrder = function(){

    var vis = this;

    var t = vis.svg.transition().duration(2000);

    if (vis.selectedSort == "sortinit_col") { // initial sort on cols (alphabetically if produced like this)
        t.selectAll(".cell")
            .attr("x", function(d) {
                var col = parseInt(d3.select(this).attr("col"));
                return col * vis.cellSize;
            });
        t.selectAll(".colLabel")
            .attr("y", function(d, i) {
                return i * vis.cellSize;
            })
            .attr("transform", function(d, i) {
                return "translate(" + vis.cellSize / 2 + ", -3) rotate(-90) rotate(45, 0, " + (i * vis.cellSize) + ")";
            });
    } else if (vis.selectedSort == "sortinit_row") { // initial sort on rows (alphabetically if produced like this)
        t.selectAll(".cell")
            .attr("y", function(d) {
                var row = parseInt(d3.select(this).attr("row"));
                return row * vis.cellSize;
            });
        t.selectAll(".rowLabel")
            .attr("y", function(d, i) {
                return i * vis.cellSize;
            })
            .attr("transform", function(d, i) {
                return "translate(-3," + vis.cellSize / 1.5 + ") rotate(-45, 0, " + (i * vis.cellSize) + ")";
            });
    } else if (vis.selectedSort == "sortinit_col_row") { // initial sort on rows and cols (alphabetically if produced like this)
        t.selectAll(".cell")
            .attr("x", function(d) {
                var col = parseInt(d3.select(this).attr("col"));
                return col * vis.cellSize;
            })
            .attr("y", function(d) {
                var row = parseInt(d3.select(this).attr("row"));
                return row * vis.cellSize;
            });
        t.selectAll(".colLabel")
            .attr("y", function(d, i) {
                return i * vis.cellSize;
            })
            .attr("transform", function(d, i) {
                return "translate(" + vis.cellSize / 2 + ", -3) rotate(-90) rotate(45, 0, " + (i * vis.cellSize) + ")";
            });
        t.selectAll(".rowLabel")
            .attr("y", function(d, i) {
                return i * vis.cellSize;
            })
            .attr("transform", function(d, i) {
                return "translate(-3," + vis.cellSize / 1.5 + ") rotate(-45, 0, " + (i * vis.cellSize) + ")";
            });
    }
};

/*
 * Data wrangling
 */
HeatMap.prototype.wrangleData = function(){

    var vis = this;

    // Select the dataset based on user' selection
    if(vis.selectedIndicator == "Deaths") {
        vis.data = vis.deaths;
    } else if(vis.selectedIndicator == "Infections") {
        vis.data = vis.new_hiv_infections;
    } else if(vis.selectedIndicator == "People with HIV") {
        vis.data = vis.people_living_with_hiv;
    }

    // Filter the data based on user's selection (Region or Agegroup)
    vis.displayData = vis.data.filter(function(d) {return (d.Region == vis.selectedRegion && d.Subgroup == vis.selectedGroup)});

    // Prepare the data structure for the visual
    vis.formattedData.columns = vis.displayData.map(function(d) { return +formatDate(d.Year); }).filter(function(item, i, c){ return i == c.indexOf(item); }).sort();
    vis.formattedData.index = vis.displayData.map(function(d) { return d.Country; }).filter(function(item, i, c){ return i == c.indexOf(item); }).sort();

    // Timeseries data has gaps, loop thru the dataset to fill the gaps with Null value
    var cntry = [];
    for (row = 0; row < vis.formattedData.index.length; row++){
        var yrs = [];
        for (col = 0; col < vis.formattedData.columns.length; col++){
            var missingValue = true;
            vis.displayData.forEach(function(d){
                if (d.Country == vis.formattedData.index[row] && +formatDate(d.Year) == vis.formattedData.columns[col]) {
                    missingValue = !missingValue;
                    yrs.push(d.Value);
                }
            });
            if (missingValue) {
                yrs.push(null);
            }
        }
        cntry.push(yrs);
    }

    vis.formattedData.data = cntry;

    // Update the visualization
    vis.updateVis();

};

/*
 * The function - use the D3 update sequence (enter, update, exit)
 */
HeatMap.prototype.updateVis = function(){

    var vis = this;

    var t = vis.svg.transition().duration(2000);

    // Update the color scale
    vis.colorScale = d3.scale.quantile()
        .domain([0, d3.max(vis.displayData, function(d) { return d.Value; })])
        .range(vis.colors);

    // Update variables
    vis.rowLabel = vis.formattedData.index;
    vis.colLabel = vis.formattedData.columns;
    vis.col_number = vis.colLabel.length;
    vis.row_number = vis.rowLabel.length;
    vis.width = vis.cellSize * vis.col_number + 80; //- vis.margin.left - vis.margin.right;
    vis.height = vis.cellSize * vis.row_number + 350; //- vis.margin.top - vis.margin.bottom;

    // Update SVG height & width
    vis.svg
        .attr("width", vis.width + vis.margin.left + vis.margin.right)
        .attr("height", vis.height + vis.margin.top + vis.margin.bottom);
        //.attr("transform", "translate(" + vis.margin.left + "," + vis.margin.top + ")");

    // Data Mapping
    var rowLabels = vis.svg.selectAll(".rowLabel")
        .data(vis.formattedData.index);

    // Enter
    rowLabels.enter().append("text")
        .attr("class", "rowLabel mono")
        .attr("id", function(d, i) { return "rowLabel_" + i; });

    // Update
    rowLabels.transition().duration(2000)
        .text(function(d) { return d; })
        .attr("x", 0)
        .attr("y", function(d, i) { return (i * vis.cellSize); })
        .style("text-anchor", "end")
        .attr("transform", function(d, i) { return "translate(-3," + vis.cellSize / 1.5 + ") rotate(-45, 0, " + (i * vis.cellSize) + ")"; });

    rowLabels.on('mouseover', function(d, i) { d3.select('#rowLabel_' + i).classed("hover", true); })
        .on('mouseout', function(d, i) { d3.select('#rowLabel_' + i).classed("hover", false); })
        .on("click", function(d, i) {
            vis.rowSortOrder = !vis.rowSortOrder;
            vis.sortByLabels("r", i, vis.rowSortOrder);
            d3.select("#order").property("selectedIndex", 0);
        });

    // Exit
    rowLabels.exit().remove();

    // Data mapping
    var colLabels = vis.svg.selectAll(".colLabel")
        .data(vis.formattedData.columns);

    // Enter
    colLabels.enter().append("text")
        .attr("class", "colLabel mono")
        .attr("id", function(d, i) { return "colLabel_" + i; });

    // Update
    colLabels.transition().duration(2000)
        .text(function(d) { return d; })
        .attr("x", 0)
        .attr("y", function(d, i) { return (i * vis.cellSize); })
        .style("text-anchor", "left")
        .attr("transform", function(d, i) { return "translate(" + vis.cellSize / 2 + ", -3) rotate(-90) rotate(45, 0, " + (i * vis.cellSize) + ")"; });

    colLabels.on('mouseover', function(d, i) { d3.select('#colLabel_' + i).classed("hover", true); })
        .on('mouseout', function(d, i) { d3.select('#colLabel_' + i).classed("hover", false); })
        .on("click", function(d, i) {
            vis.colSortOrder = !vis.colSortOrder;
            vis.sortByLabels("c", i, vis.colSortOrder);
            d3.select("#sort-heatmap").property("selectedIndex", 0);
        });

    // Exit
    colLabels.exit().remove();

    // Data Mapping
    var row = vis.svg.selectAll(".row")
        .data(vis.formattedData.data);

    // Enter
    row.enter().append("g")
        .attr("id", function(d) { return d.idx; })
        .attr("class", "row");

    // Exit
    row.exit().remove();

    var j = 0;

    // Data mapping
    var heatMap = row.selectAll(".cell")
        .data(function(d) {
            j++;
            return d;
        });

    // Enter
    heatMap.enter().append("svg:rect")
        .attr("class", function(d, i, j) { return "cell bordered cr" + j + " cc" + i; })
        .attr("rx", 4)
        .attr("ry", 4)
        .attr("row", function(d, i, j) { return j; })
        .attr("col", function(d, i, j) { return i; })
        .attr("width", vis.cellSize)
        .attr("height", vis.cellSize);

    // Update
    heatMap
        .attr("x", function(d, i) { return i * vis.cellSize; })
        .attr("y", function(d, i, j) { return j * vis.cellSize; })
        .style("fill", vis.colorScale(0));

    heatMap.transition().duration(1000)
        .style("fill", function(d) { if (d != null) return vis.colorScale(d); else return "url(#diagonalHatch)"; });

    heatMap.on('mouseover', function(d, i, j) {
        d3.select('#colLabel_' + i).classed("hover", true);
        d3.select('#rowLabel_' + j).classed("hover", true);
        d3.select(this).classed("cell-hover",true);
        d3.select(this).transition().duration(300).style("opacity", 0.8);
        if (d != null) {
            vis.tooltip.show("<b>" + numFormat(d) + " </b>" + vis.selectedIndicator);
        } else {
            vis.tooltip.show("<b>No Data Available</b>");
        }
        })
        .on('mouseout', function(d, i, j) {
            d3.select('#colLabel_' + i).classed("hover", false);
            d3.select('#rowLabel_' + j).classed("hover", false);
            d3.select(this).classed("cell-hover",false);
            d3.select(this).transition().duration(300).style("opacity", 1);
            vis.tooltip.hide();
        })
        .on("mousemove", function(d, i) {
            vis.tooltip.move();
        })
        .on('click', function() {
            //console.log(d3.select(this));
        });

    // Exit
    heatMap.exit().remove();

    // Remove legends
    vis.svg.selectAll(".legend").remove();

    // Add Legends
    var legend = vis.svg.append("g")
        .attr("class", "legend")
        //.attr("transform", "translate(0,-200)")
        .attr("transform", "translate(" + vis.width/5 + ",-200)")
        .selectAll(".legendElement")
        .data([0].concat(vis.colorScale.quantiles()), function(d) { return d; })
        .enter().append("g")
        .attr("class", "legendElement");

    legend.append("svg:rect")
        .attr("x", function(d, i) { return vis.legendElementWidth * i; })
        .attr("y", vis.viewerPosTop)
        .attr("class", "cellLegend bordered")
        .attr("width", vis.legendElementWidth)
        .attr("height", vis.cellSize / 2)
        .style("fill", function(d, i) { return vis.colors[i]; });

    legend.append("text")
        .attr("class", "mono legendElement legendText")
        .text(function(d) { return formatAbbreviation(Math.round(d)); })
        .attr("x", function(d, i) { return vis.legendElementWidth * i; })
        .attr("y", vis.viewerPosTop + vis.cellSize);

};
