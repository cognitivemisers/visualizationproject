/**
 * Created by lucerne on 4/15/16.
 */

/*
 *  CircleVis - Object constructor function
 *  @param _parentElement   -- HTML element in which to draw the visualization
 *  @param _data1            -- Array with future death data
 *  @param _data2            -- Array with future hiv data
 */

CircleVis = function(_parentElement, _Data1, _Data2) {

    this.parentElement = _parentElement;
    this.deathData = _Data1;
    this.hivData = _Data2;
    this.initVis();
};


/**
 *  Initialize circle plot
 */

CircleVis.prototype.initVis = function() {
    var vis = this;

    vis.margin = {top: 20, right: 20, bottom: 50, left: 110};
    vis.width = 850 - vis.margin.left - vis.margin.right;
    vis.height = 400 - vis.margin.top - vis.margin.bottom;

    vis.svg = d3.select("#" + vis.parentElement).append("svg")
        .attr("width", vis.width + vis.margin.left + vis.margin.right)
        .attr("height", vis.height + vis.margin.top + vis.margin.bottom)
        .append("g")
        .attr("transform", "translate(" + vis.margin.left + "," + vis.margin.top + ")");

    vis.x = d3.time.scale()
        .range([0, vis.width])
        .domain(d3.extent(vis.deathData, function(d) { return d.Year; }));

    vis.y = d3.scale.linear()
        .domain([0,1,2,3])
        .range([vis.height, 0]);

    // tool tip
    var tipFT = d3.tip()
    	.attr('class', 'd3-tip')
    	.html(function(d) {
    		return d.FastTrack + " Million";
    	});
    var tipNFT = d3.tip()
        .attr('class', 'd3-tip')
        .html(function(d) {
            return d.NoFastTrack + " Million";
        });

    vis.circleSpacing = 50;
    vis.axisSpacing = 80;
    vis.radiusDeath = 8.5;
    vis.radiusHiv = 7.8;

    // blue, light blue, red, light red
    vis.colorScale = {"death" : {"FastTrack": "#1f78b4", "NoFastTrack": "#a6cee3"},
        "hiv": {"FastTrack": "#e31a1c", "NoFastTrack": "#fb9a99"}}

    // Fast Track
    vis.circle = vis.svg.selectAll("circle-DeathFT")
        .data(vis.deathData);

    vis.circle.enter().append("circle")
        .attr("class", "dot future-dot");

    vis.circle
        .attr("r", function(d){return d.FastTrack*vis.radiusDeath;})
        .attr("cx", function(d, i){return vis.x(d.Year); })
        .attr("cy", vis.circleSpacing + vis.axisSpacing)
        .attr("fill", function(d){return vis.colorScale.death.FastTrack;})
        .on('mouseover', function(d){ tipFT.show(d);})
        .on('mouseout', tipFT.hide)
        .call(tipFT)
        .on('click', function(d,i){futureState[1].value.updateVis("death",i)});

    // No Fast Track
    vis.circle = vis.svg.selectAll("circle-deathNFT")
        .data(vis.deathData);

    vis.circle.enter().append("circle")
        .attr("class", "dot future-dot");

    vis.circle
        .attr("r", function(d){return d.NoFastTrack*vis.radiusDeath;})
        .attr("cx", function(d, i){return vis.x(d.Year); })
        .attr("cy", vis.circleSpacing*2 + vis.axisSpacing)
        .attr("fill", function(d){return vis.colorScale.death.NoFastTrack;})
        .on('mouseover', tipNFT.show)
        .on('mouseout', tipNFT.hide)
        .call(tipNFT)
        .on('click', function(d,i){futureState[1].value.updateVis("death",i)});

    // Fast Track
    vis.circle = vis.svg.selectAll("circle-HivFT")
        .data(vis.hivData);

    vis.circle.enter().append("circle")
        .attr("class", "dot future-dot");

    vis.circle
        .attr("r", function(d){return d.FastTrack*vis.radiusHiv;})
        .attr("cx", function(d, i){return vis.x(d.Year); })
        .attr("cy", vis.circleSpacing*3 + vis.axisSpacing)
        .attr("fill", function(d){return vis.colorScale.hiv.FastTrack;})
        .on('mouseover', tipFT.show)
        .on('mouseout', tipFT.hide)
        .call(tipFT)
        .on('click', function(d,i){futureState[1].value.updateVis("hiv",i)});

    // No Fast Track
    vis.circle = vis.svg.selectAll("circle-HivNFT")
        .data(vis.hivData);

    vis.circle.enter().append("circle")
        .attr("class", "dot future-dot");

    vis.circle
        .attr("r", function(d){return d.NoFastTrack*vis.radiusHiv;})
        .attr("cx", function(d, i){return vis.x(d.Year); })
        .attr("cy", vis.circleSpacing*4 + vis.axisSpacing)
        .attr("fill", function(d){return vis.colorScale.hiv.NoFastTrack;})
        .on('mouseover', tipNFT.show)
        .on('mouseout', tipNFT.hide)
        .call(tipNFT)
        .on('click', function(d,i){futureState[1].value.updateVis("hiv",i)});


     //circle legend
    vis.legendData = ["Death, FastTrack", "New HIV Infection, FastTrack"];
    vis.circleLegend = vis.svg.selectAll("circle-legend")
        .data(vis.legendData);

    vis.circleLegend.enter().append("circle")
        .attr("class", "dot");

    vis.circleLegend
        .attr("r", function(d){return 5;})
        .attr("cx", function(d) {
            if (d == "Death, FastTrack") {
                return vis.width*2/8;
            }
            else {
                return vis.width/2;
            }
        })
        .attr("cy",  function(d) {return vis.circleSpacing*5 + vis.axisSpacing;})
        .attr("fill", function(d){
            if (d == "Death, FastTrack") {
                return vis.colorScale.death.FastTrack;
            }
            else{
                return vis.colorScale.hiv.FastTrack;
            }
        });

    vis.svg.selectAll("text.value")
        .data(vis.legendData)
        .enter()
        .append("text")
        .attr("class", "future-circle-legend-label")
        .text(function(d){
            return d;
        })
        .attr("x", function(d) {
            if (d == "Death, FastTrack") {
                return vis.width*2/8+20;
            }
            else {
                return vis.width/2+20;
            }
        })
        .attr("y", function (d, index) {
            return vis.circleSpacing*5 + vis.axisSpacing+5;
        })
        .attr("fill",function(d) {
            if (d == "Death, FastTrack") {
                return vis.colorScale.death.FastTrack;
            }
            else {
                return vis.colorScale.hiv.FastTrack;
            }
        });


    //circle legend
    vis.legendData = ["Death, No FastTrack", "New HIV Infection, No FastTrack"];
    vis.circleLegend = vis.svg.selectAll("circle-legend")
        .data(vis.legendData);

    vis.circleLegend.enter().append("circle")
        .attr("class", "dot");


    vis.circleLegend
        .attr("r", function(d){return 5;})
        .attr("cx", function(d) {
            if (d == "Death, No FastTrack") {
                return vis.width*2/8;
            }
            else {
                return vis.width/2;
            }
        })
        .attr("cy",  function(d) {return vis.circleSpacing*5 + vis.axisSpacing+20;})
        .attr("fill", function(d){
            if (d == "Death, No FastTrack") {
                return vis.colorScale.death.NoFastTrack;
            }
            else{
                return vis.colorScale.hiv.NoFastTrack;
            }
        });

    vis.svg.selectAll("text.value")
        .data(vis.legendData)
        .enter()
        .append("text")
        .attr("class", "future-circle-legend-label")
        .text(function(d){
            return d;
        })
        .attr("x", function(d) {
            if (d == "Death, No FastTrack") {
                return vis.width*2/8+20;
            }
            else {
                return vis.width/2+20;
            }
        })
        .attr("y", function (d, index) {
            return vis.circleSpacing*5 + vis.axisSpacing+5+20;
        })
        .attr("fill", function(d){
            if (d == "Death, No FastTrack") {
                return vis.colorScale.death.NoFastTrack;
            }
            else{
                return vis.colorScale.hiv.NoFastTrack;
            }
        });

    vis.xAxis = d3.svg.axis()
        .scale(vis.x)
        .orient("top");

    vis.yAxis = d3.svg.axis()
        .scale(vis.y)
        .orient("left");

    vis.svg.append("g")
        .attr("class", "x-axis-future axis-future")
        .attr("transform", "translate(0," + vis.axisSpacing + ")");

    vis.wrangleData();
};


/*
 *  Data wrangling
 */

CircleVis.prototype.wrangleData = function() {
    var vis = this;

    vis.updateVis();
};

/*
 *  The drawing function
 */

CircleVis.prototype.updateVis = function() {
    var vis = this;

    // Call axis functions with the new domain
    vis.svg.select(".x-axis-future").call(vis.xAxis);
};


/*
 * AreaChart - Object constructor function
 * @param _parentElement 	-- the HTML element in which to draw the visualization
 * @param _data1			-- death data array of FastTrack, NoFastTrack, Year
 * @param _data2			-- hiv data array of FastTrack, NoFastTrack, Year
 */

AreaChart = function(_parentElement, _data1, _data2){
    this.parentElement = _parentElement;
    this.data = _data1;
    this.deathData = _data1;
    this.hivData = _data2;
    this.displayData = []; // see data wrangling
    this.circleHover = false;
    this.circleData = [];
    this.chartType = "death";

    // DEBUG RAW DATA
    //console.log(this.data);

    this.initVis();
};

/*
 * Initialize visualization
 */

AreaChart.prototype.initVis = function() {
    var vis = this;

    vis.margin = {top: 40, right: 20, bottom: 20, left: 30};

    vis.width = 450 - vis.margin.left - vis.margin.right;
    vis.height = 300 - vis.margin.top - vis.margin.bottom;


    // SVG drawing area
    vis.svg = d3.select("#" + vis.parentElement).append("svg")
        .attr("width", vis.width + vis.margin.left + vis.margin.right)
        .attr("height", vis.height + vis.margin.top + vis.margin.bottom)
        .append("g")
        .attr("transform", "translate(" + vis.margin.left + "," + vis.margin.top + ")");

    vis.categoryName = vis.svg.append("g")
        .attr("id", "text.name")
        .append("text")
        .text("")
        .attr("transform",
            function (d) {
                return "translate(20,0)";
            });

    vis.bindData();

};

/*
 * bindData:        -- Switching between two AreaChart
 */

AreaChart.prototype.bindData = function(){
    var vis = this;

    if (vis.chartType == "death"){
        vis.data = vis.deathData;
    }
    else{
        vis.data = vis.hivData;
    }

    // Scales and axes
    vis.x = d3.time.scale()
        .range([0, vis.width])
        .domain(d3.extent(vis.data, function(d) { return d.Year; }));

    vis.y = d3.scale.linear()
        .range([vis.height, 0]);

    vis.xAxis = d3.svg.axis()
        .scale(vis.x)
        .orient("bottom");

    vis.yAxis = d3.svg.axis()
        .scale(vis.y)
        .orient("left");

    vis.svg.append("g")
        .attr("class", "x-axis-future axis-future")
        .attr("transform", "translate(0," + vis.height + ")");

    vis.svg.append("g")
        .attr("class", "y-axis-future axis-future");


    // Define two colors
    vis.colorScale = d3.scale.category20();

    vis.colorScale.domain(d3.keys(vis.data[0]).filter(function (d) {
        return d != "Year";
    }));

    vis.dataCategories = vis.colorScale.domain();
    //console.log(vis.colorScale.range());

    vis.colorScale = {"death" : {"FastTrack": "#1f78b4", "NoFastTrack": "#a6cee3"},
        "hiv": {"FastTrack": "e31a1c", "NoFastTrack": "#fb9a99"}}

    // Rearrange data into data series
    vis.transposedData = vis.dataCategories.map(function(name) {
        return {
            name: name,
            values: vis.data.map(function(d) {
                return {Year: d.Year, y: d[name]};
            }) };
    });

    var stack = d3.layout.stack()
        .values(function(d) { return d.values; });

    vis.stackedData = stack(vis.transposedData);

    var minYear = d3.min(vis.stackedData, function(d) {
        return d.values[0].Year;
    });

    var maxYear = d3.max(vis.stackedData, function(d) {
        var last = d.values.length-1;
        return d.values[last].Year;
    });

    // What is max data? max data = max of sum of data points
    var lastCategory = vis.stackedData.length-1;
    var lastValue = vis.stackedData[lastCategory].values.length-1;
    vis.maxData = vis.stackedData[lastCategory].values[lastValue];

    //vis.maxData = vis.maxData.y + vis.maxData.y0;
    vis.maxData = vis.maxData.y;

    // in Tue Jan 01 1974 00:00:00 GMT-0800 (PST) format
    vis.x = d3.time.scale()
        .domain([minYear, maxYear])
        .range([0, vis.width]);

    // Area series chart
    vis.area = d3.svg.area()
        .interpolate("cardinal")
        .x(function(d) {
            return vis.x(d.Year); })
        .y0(function(d) {
            return vis.y(d.y0); })
        //.y1(function(d) { return vis.y(d.y0 + d.y); });
        .y1(function(d) { return vis.y(d.y); });

    vis.filterArea = d3.svg.area()
        .interpolate("cardinal")
        .x(function(d) {
            return vis.x(d.Year); })
        .y0(function(d) {
            return vis.y(0); })
        .y1(function(d) { return vis.y(d.y); });

    vis.wrangleData();

    vis.filter = "";
};


AreaChart.prototype.bindEventListeners = function () {
    var vis = this;

    $('input[name="f-future-line"]').change( function() {
        vis.chartType = $(this).val();
        vis.bindData();
    });

};

/*
 * Data wrangling
 */

AreaChart.prototype.wrangleData = function(){
    var vis = this;

    vis.filteredData;
    // In the first step no data wrangling/filtering needed
    if (vis.filter){
        // Get the name of the category selected
        vis.displayData = vis.stackedData.filter(function(d) { return d.name == vis.filter } );

    }
    else {
        vis.displayData = vis.stackedData;
    }

    // Update the visualization
    vis.updateVis();
}



/*
 * The drawing function - should use the D3 update sequence (enter, update, exit)
 * Function parameters only needed if different kinds of updates are needed
 */

AreaChart.prototype.updateVis = function(){
    var vis = this;

    // Update domain
    // Get the maximum of the multi-dimensional array or in other words, get
    // the highest peak of the uppermost layer

    if(vis.filter) {
        vis.y.domain([0, d3.max(vis.displayData[0].values,function(d) { return d.y; })]);
    } else {
        vis.y.domain([0, d3.max(vis.displayData, function(d) {
            return d3.max(d.values, function(e) {
                //return e.y0 + e.y;
                return e.y;
            });
        })
        ]);
    }

    // Draw the layers
    vis.categories = vis.svg.selectAll(".area")
        .data(vis.displayData);

    vis.categories.enter().append("path")
        .attr("class", "area");

    vis.categories
        .style("fill", function(d) {
            return vis.colorScale[vis.chartType][d.name];
        })
        .attr("d", function(d) {
            if(vis.filter) {
                return vis.filterArea(d.values);
            }
            else {
                return vis.area(d.values);
            }
        })
        .on('mouseover', function(d){
            if (d.name == "NoFastTrack"){
                vis.categoryName.text("No FastTrack");
            }
            else {
                vis.categoryName.text(d.name);
            }
        })
        .on('mouseout', function(d){vis.categoryName.text("");})
        .on("click", function(d) {
            vis.filter = (vis.filter == d.name) ? "" : d.name;
            vis.wrangleData();
        });


    // TO-DO: Update tooltip text
    vis.categories.exit().remove();

    vis.y = d3.scale.linear()
        .domain([0, vis.maxData])
        .range([vis.height, 0]);


    vis.svg.append("text")
        .attr("class", "axis-future-text y-axis-future-text")
        .attr("text-anchor", "end")
        .attr("x", -10)
        .attr("y", 15)
        .attr("transform", "rotate(-90)")
        .text("Million");


    // Call axis functions with the new domain
    vis.svg.select(".x-axis-future").call(vis.xAxis);
    vis.svg.select(".y-axis-future").call(vis.yAxis);
};

/*
 *  PeopleVis - Object constructor function
 *  @param _parentElement   -- HTML element in which to draw the visualization
 *  @param _data            -- Array with future difference data
 */

PeopleVis = function(_parentElement, _Data) {

    this.parentElement = _parentElement;
    this.data = _Data;
    this.peopleNum = [];
    this.initVis();
};

/*
 *  Initialize people plot
 */

PeopleVis.prototype.initVis = function() {
    var vis = this;

    vis.margin = {top: 20, right: 0, bottom: 20, left: 20};
    vis.width = 270 - vis.margin.left - vis.margin.right;
    vis.height = 100 - vis.margin.top - vis.margin.bottom;

    vis.svg = d3.select("#" + vis.parentElement).append("svg")
        .attr("width", vis.width + vis.margin.left + vis.margin.right)
        .attr("height", vis.height + vis.margin.top + vis.margin.bottom)
        .append("g")
        .attr("transform", "translate(" + vis.margin.left + "," + vis.margin.top + ")");

    // calculate and store number of people to display
    var i, death, hiv;
    for (i=0; i < vis.data.length; i++) {
        death = Math.round(vis.data[i]["death"] / vis.data[vis.data.length - 1]["death"] * 10);
        hiv = Math.round(vis.data[i]["hiv"] / vis.data[vis.data.length - 1]["hiv"] * 10);
        vis.peopleNum.push({"death": death, "hiv": hiv});
    }

    vis.updateVis("death",0);
};

/**
 *
 * @param name      -- death or hiv selected
 * @param index     -- index of data array selected
 */

PeopleVis.prototype.updateVis = function(name,index) {
    var vis = this;

    var indexArray;
    d3.select("#future-people-in-million").text("");
    d3.select("#future-fasttrack").text("");
    if (vis.peopleNum[index][name] > 0){
        d3.select("#future-people-in-million")
            .text((vis.data[index][name]).toFixed(1) + " Million");

        indexArray = new Array(vis.peopleNum[index][name]);

        if (name == "death"){
            d3.select("#future-fasttrack")
                .text("Less Deaths with Fast Track");
        }
        else{
            d3.select("#future-fasttrack")
                .text("Less Infections with Fast Track");
        }
    }
    else{
        indexArray = [];
    }


    vis.people = vis.svg.selectAll(".text-people")
        .data(indexArray);

    vis.people.exit().remove();

    vis.people.enter().append("text")
        .attr("class", "text-people");

    vis.colorScale = {death: "#1f78b4", hiv: "#e31a1c"};

    vis.people
        .attr('font-family', 'FontAwesome')
        .text(function(d) { return '\uf183' })
        .attr("font-size",40)
        .attr("fill", function(d){
            if (name == "death") {
                return vis.colorScale.death;
            }
            else{
                return vis.colorScale.hiv;
            }
        })
        .attr("x", function(d,i) {
            return i*25;
        })
        .attr("y", function (d, i) {
            return 40;
        });

    vis.svg.selectAll(".text-people")
        .style("opacity", 0)
        .transition()
        .duration(800)
        .style("opacity", 1);

};