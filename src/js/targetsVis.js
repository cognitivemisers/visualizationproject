// Create SVG drawing space
var margin = {top: 5, right: 5, bottom: 5, left: 5},
    width = 400 - margin.left - margin.right,
    height = 450 - margin.top - margin.bottom;

var svgNum = d3.select("#targets-vis").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// Draw logo - blue arrow with circles
svgNum.append("circle").attr("class", "logoR").attr("cx", 5).attr("cy", 5).attr("r", 6);
svgNum.append("circle").attr("class", "logoR").attr("cx", 20).attr("cy", 5).attr("r", 6);
svgNum.append("circle").attr("class", "logoR").attr("cx", 20).attr("cy", 20).attr("r", 6);
svgNum.append("circle").attr("class", "logoR").attr("cx", 35).attr("cy", 20).attr("r", 6);
svgNum.append("circle").attr("class", "logoR").attr("cx", 35).attr("cy", 35).attr("r", 6);
svgNum.append("circle").attr("class", "logoR").attr("cx", 50).attr("cy", 35).attr("r", 6);
svgNum.append("circle").attr("class", "logoR").attr("cx", 35).attr("cy", 50).attr("r", 6);
svgNum.append("circle").attr("class", "logoR").attr("cx", 20).attr("cy", 50).attr("r", 6);
svgNum.append("circle").attr("class", "logoR").attr("cx", 20).attr("cy", 65).attr("r", 6);
svgNum.append("circle").attr("class", "logoR").attr("cx", 5).attr("cy", 65).attr("r", 6);

// Draw logo - red arrow with circles
svgNum.append("circle").attr("class", "logoB").attr("cx", 45).attr("cy", 5).attr("r", 6);
svgNum.append("circle").attr("class", "logoB").attr("cx", 60).attr("cy", 5).attr("r", 6);
svgNum.append("circle").attr("class", "logoB").attr("cx", 60).attr("cy", 20).attr("r", 6);
svgNum.append("circle").attr("class", "logoB").attr("cx", 75).attr("cy", 20).attr("r", 6);
svgNum.append("circle").attr("class", "logoB").attr("cx", 75).attr("cy", 35).attr("r", 6);
svgNum.append("circle").attr("class", "logoB").attr("cx", 90).attr("cy", 35).attr("r", 6);
svgNum.append("circle").attr("class", "logoB").attr("cx", 75).attr("cy", 50).attr("r", 6);
svgNum.append("circle").attr("class", "logoB").attr("cx", 60).attr("cy", 50).attr("r", 6);
svgNum.append("circle").attr("class", "logoB").attr("cx", 60).attr("cy", 65).attr("r", 6);
svgNum.append("circle").attr("class", "logoB").attr("cx", 45).attr("cy", 65).attr("r", 6);

// Add Targets Title
svgNum.append("text").attr("class", "FTTitle").attr("x", 110).attr("y", 45).text("Fast-Track Targets");

// Add Targets Sub Title
svgNum.append("text").attr("class", "FTSubTitle").attr("x", 5).attr("y", 150).text("by 2020");
svgNum.append("text").attr("class", "FTSubTitle").attr("x", 225).attr("y", 150).text("by 2030");

// Add Treatment Targets
svgNum.append("text").attr("class", "FTTargetR").attr("x", 5).attr("y", 200).text("90-90-90");
svgNum.append("text").attr("class", "FTTargetB").attr("x", 225).attr("y", 200).text("95-95-95");

svgNum.append("text").attr("class", "FTTargetTR").attr("x", 5).attr("y", 220).text("Treatment");
svgNum.append("text").attr("class", "FTTargetTB").attr("x", 225).attr("y", 220).text("Treatment");

// Add New HIV infections Targets
svgNum.append("text").attr("class", "FTTargetR").attr("x", 5).attr("y", 300).text("500,000");
svgNum.append("text").attr("class", "FTTargetB").attr("x", 225).attr("y", 300).text("200,000");

svgNum.append("text").attr("class", "FTTargetTR").attr("x", 5).attr("y", 320).text("New Infections");
svgNum.append("text").attr("class", "FTTargetTB").attr("x", 225).attr("y", 320).text("New Infections");

// Add Discrimination Targets
svgNum.append("text").attr("class", "FTTargetR").attr("x", 5).attr("y", 400).text("ZERO");
svgNum.append("text").attr("class", "FTTargetB").attr("x", 225).attr("y", 400).text("ZERO");

svgNum.append("text").attr("class", "FTTargetTR").attr("x", 5).attr("y", 420).text("Discrimination");
svgNum.append("text").attr("class", "FTTargetTB").attr("x", 225).attr("y", 420).text("Discrimination");
