/**
 * @file Current State Visualization
 * @author Umair Cheema
 */

/*
 * Current State Visualization - Object constructor function
 * @param _parentElement 	-- the HTML element in which to draw the visualization
 * @param _data				-- the data array containing individual datasets needed
 */
CurrentStateVisualization = function (_parentElement, _data) {
    this.parentElement = _parentElement;
    this.regionsMapData = _data[0];
    this.deathsData = _data[1];
    this.deathsCountryData = _data[2];
    this.infectionsData = _data[3];
    this.infectionsCountryData = _data[4];
    this.yearSlider = null;
    this.selectedYear = "Current";
    this.selectedRegion = "World";
    this.selectedGroup = "All ages";
    this.selectedIndicator = "Deaths";
    this.colorScale = d3.scale.quantize()
        .range(colorbrewer.Paired[6]);
    this.mapData = null;
    this.mapsvg = null;
    this.mapPath = null;
    this.filteredData = null;
    this.pathsbounded = null;
    this.boundaries = null;
    this.mapWidth = null;
    this.mapHeight = null;
    this.eventType = 'time';
    this.timesvg = null;
    this.parseDate = d3.time.format("%Y").parse;
    this.timeSeriesCountriesData = null;
    this.timeSeriesRegionsData = null;
    this.playing = false;
    this.max = 0;
    this.min = 0;

}


/*
 * Initialize DOM
 */
CurrentStateVisualization.prototype.initDOM = function () {
    var vis = this;
    $.get("includes/current.html", function (data) {
        $("#current-vis").append(data);
        vis.initializeViewState();
        vis.bindEventListeners();
        vis.initializeMap();
        vis.initializeTimeSeries();
        vis.initializeDescriptiveStats();
        vis.updateMapView();
        vis.updateTimeSeriesView();
        vis.updateDescriptiveStats();
    });


}

/*
 * Initializes the view state
 */
CurrentStateVisualization.prototype.initializeViewState = function () {
    var vis = this;
    vis.yearSlider = $("#mapYearSlider").slider({
        min: 2000,
        max: 2014,
        step: 1,
        tooltip: "hide",
        natural_arrow_keys: true,
        value: 2014
    });

    $('#indicator-year').html(this.selectedYear);
    //Toggle initial button
    $("#map-primary-indicator").addClass('active');

}

/*
 * Register Event Listeners
 */
CurrentStateVisualization.prototype.bindEventListeners = function () {
    var vis = this;
    $("#mapYearSlider").on("change", function (slideEvt) {
        vis.selectedYear = slideEvt.value.newValue;
        $('#indicator-year').html(vis.selectedYear);
        vis.eventType = 'time';
        vis.updateMapView();
        vis.updateTimeSeriesView();
        vis.updateDescriptiveStats();
    });

    $('input[name="m-regiongroup"]').change(function () {
        vis.selectedRegion = $(this).val();
        vis.eventType = 'region';
        vis.updateMapView();
        vis.updateTimeSeriesView();
        vis.updateDescriptiveStats();

    })

    $('input[name="m-agegroup"]').change(function () {
        vis.selectedGroup = $(this).val();
        vis.eventType = 'group';
        vis.updateMapView();
        vis.updateTimeSeriesView();
        vis.updateDescriptiveStats();

    })

    $("#map-btn-indicator .btn").click(function () {
        $(this).button('toggle');
        $(this).siblings('.btn').removeClass('active');
        vis.selectedIndicator = $(this).attr("value");
        vis.eventType = 'indicator';
        vis.updateMapView();
        vis.updateTimeSeriesView();
        vis.updateDescriptiveStats();
    });

    $('#playButton').click(function () {

        vis.playing = true;
        if (vis.selectedYear == "Current") {
            vis.selectedYear = "1000";
        } else {
            vis.selectedYear = new String(parseInt(vis.selectedYear) + 1);
        }
        vis.yearSlider.slider('setValue', parseInt(vis.selectedYear), false, true);

        if (vis.playing) {
            if (vis.selectedYear == "2014") {
                vis.selectedYear = "Current";
            }
            setTimeout(playAnimation, 1000);

        }


    });
    $('#pauseButton').click(function () {
        vis.playing = false;
    });

    function playAnimation() {
        if (vis.playing) {
            $('#playButton').trigger("click");
        }
    }
}


/**
 * Initialize Descriptive Stats
 * Visualization
 */

CurrentStateVisualization.prototype.initializeDescriptiveStats = function () {
    var vis = this;
    vis.descriptiveStatsWidth = $("#descriptive_stats").width();
    vis.descriptiveStatsHeight = vis.mapHeight - 100;

    vis.statssvg = d3.select("#descriptive_stats").append('svg')
        .attr('width', vis.descriptiveStatsWidth - 6)
        .attr('height', vis.descriptiveStatsHeight - 6);

    vis.maxCircle = vis.statssvg.append('circle')
        .attr("r", (vis.descriptiveStatsWidth - 14) / 2)
        .attr("cx", (vis.descriptiveStatsWidth - 4) / 2)
        .attr("cy", 100)
        .attr("id", "max1")
        .attr("fill", "#e31a1c");


    vis.minCircle = vis.statssvg.append('circle')
        .attr("r", (vis.descriptiveStatsWidth - 14) / 2)
        .attr("cx", (vis.descriptiveStatsWidth - 4) / 2)
        .attr("cy", 150 + vis.descriptiveStatsWidth)
        .attr("id", "min1")
        .attr("fill", "#a6cee3");


    vis.maxCircleInternal = vis.statssvg.append('circle')
        .attr("r", (vis.descriptiveStatsWidth - 30) / 2)
        .attr("cx", (vis.descriptiveStatsWidth - 4) / 2)
        .attr("cy", 100)
        .attr("id", "max2")
        .attr("fill", "#eeeeee");

    vis.minCircleInternal = vis.statssvg.append('circle')
        .attr("r", (vis.descriptiveStatsWidth - 30) / 2)
        .attr("cx", (vis.descriptiveStatsWidth - 4) / 2)
        .attr("cy", 150 + vis.descriptiveStatsWidth)
        .attr("id", "min2")
        .attr("fill", "#eeeeee");


    //Append Titles

    vis.statssvg.append('text')
        .attr("x", 15)
        .attr("class", "picaption")
        .attr("y", 180)
        .text("Maximum");

    vis.statssvg.append('text')
        .attr("x", 15)
        .attr("class", "picaption")
        .attr("y", 330)
        .text("Minimum");

    //Append default minimum and maximum

    vis.statssvg.append('text')
        .attr("x", (vis.descriptiveStatsWidth - 4) / 2)
        .attr("y", 100)
        .attr("text-anchor", "middle")
        .attr("class", "bignumber")
        .attr("id", "maxtext")
        .text("0");


    vis.statssvg.append('text')
        .attr("x", (vis.descriptiveStatsWidth - 4) / 2)
        .attr("y", 150 + vis.descriptiveStatsWidth)
        .attr("text-anchor", "middle")
        .attr("class", "bignumber")
        .attr("id", "mintext")
        .text("0");


}


/**
 * Updates the descriptive stats
 */
CurrentStateVisualization.prototype.updateDescriptiveStats = function () {
    var vis = this;

    var data = vis.filteredData;
    var minimum = -1;
    var maximum = -1;
    vis.maxRegion = "unknown";
    vis.minRegion = "unknown";


    for (var property in data) {
        if (this.selectedRegion == "World") {
            if (property !== "All countries") {
                if ((minimum == -1) && (maximum == -1)) {
                    vis.minRegion = property;
                    vis.maxRegion = property;
                    minimum = data[property];
                    maximum = data[property];
                } else {

                    if (data[property] < minimum) {
                        vis.minRegion = property;
                        minimum = data[property];
                    } else if (data[property] > maximum) {
                        vis.maxRegion = property;
                        maximum = data[property];
                    }

                }
            }
        } else {
            if ((property !== "East and Southern Africa")
                && (property !== "West and Central Africa")
                && (property !== "Asia and the Pacific")
                && (property !== "Eastern Europe and Central Asia")
                && (property !== "Latin America and the Caribbean")
                && (property !== "Middle East and North Africa")
                && (property !== "Western & Central Europe and North America")
            ) {
                if ((minimum == -1) && (maximum == -1)) {
                    vis.minRegion = property;
                    vis.maxRegion = property;
                    minimum = data[property];
                    maximum = data[property];
                } else {

                    if (data[property] < minimum) {
                        vis.minRegion = property;
                        minimum = data[property];
                    } else if (data[property] > maximum) {
                        vis.maxRegion = property;
                        maximum = data[property];
                    }

                }
            }


        }
    }


    d3.select("#mintext").remove();
    vis.statssvg.append('text')
        .attr("x", (vis.descriptiveStatsWidth - 4) / 2)
        .attr("y", 150 + vis.descriptiveStatsWidth)
        .attr("text-anchor", "middle")
        .attr("class", "bignumber")
        .attr("id", "mintext")
        .text(vis.formatAbbreviation(minimum));

    d3.select("#maxtext").remove();
    vis.statssvg.append('text')
        .attr("x", (vis.descriptiveStatsWidth - 4) / 2)
        .attr("y", 100)
        .attr("text-anchor", "middle")
        .attr("class", "bignumber")
        .attr("id", "maxtext")
        .text(vis.formatAbbreviation(maximum));


    var maxobjects = ["#maxtext"];
    var minobjects = ["#mintext"];

    for (var i = 0; i < maxobjects.length; i++) {

        //Add event listener
        d3.select(maxobjects[i]).on("mouseover", function (d, i) {
            vis.maptip.transition()
                .duration(200)
                .style("opacity", .7);

            vis.maptip.html("<br/><i>" + vis.maxRegion + "</i>")
                .style("left", (d3.event.pageX + 15) + "px")
                .style("top", (d3.event.pageY - 80) + "px");

        }).on("mouseout", function (d, i) {

            vis.maptip
                .style("opacity", 0);

        }).style("cursor", "pointer");

    }

    for (var i = 0; i < minobjects.length; i++) {

        //Add event listener
        d3.select(minobjects[i]).on("mouseover", function (d, i) {
            vis.maptip.transition()
                .duration(200)
                .style("opacity", .7);

            vis.maptip.html("<br/><i>" + vis.minRegion + "</i>")
                .style("left", (d3.event.pageX + 15) + "px")
                .style("top", (d3.event.pageY - 80) + "px");

        }).on("mouseout", function (d, i) {

            vis.maptip
                .style("opacity", 0);

        }).style("cursor", "pointer");


    }
}


/**
 * Initialize the Time Series
 * Visualization
 */
CurrentStateVisualization.prototype.initializeTimeSeries = function () {
    var vis = this;
    //We need to match width to map component
    vis.margin = {top: 0, right: 0, bottom: 10, left: 10};
    vis.timeSeriesWidth = vis.mapWidth - vis.margin.left - vis.margin.right;
    vis.timeSeriesHeight = 150 - vis.margin.top - vis.margin.bottom;

    vis.timesvg = d3.select('#m_timeseries').append('svg')
        .attr('width', vis.timeSeriesWidth)
        .attr('height', vis.timeSeriesHeight)
        .append("g")
        .attr("transform", "translate(" + vis.margin.left + "," + vis.margin.top + ")");
    // Scales and axes
    vis.padding = 20;
    vis.time_x = d3.time.scale()
        .range([vis.padding, vis.timeSeriesWidth - vis.padding]);
    vis.time_y = d3.scale.linear()
        .range([vis.timeSeriesHeight - vis.padding, vis.padding]);

}

/**
 * Updates the Time series view
 */
CurrentStateVisualization.prototype.updateTimeSeriesView = function () {
    var vis = this;
    var filteredData = vis.filterDataRange(2000, 2016);

    vis.time_x.domain(d3.extent(filteredData, function (d) {

        return d.Year;
    }));

    vis.time_y.domain([0, d3.max(filteredData, function (d) {

        return d.Value;
    })]);

    //Set up axes
    vis.time_xAxis = d3.svg.axis()
        .scale(vis.time_x)
        // .tickSize(-vis.timeSeriesHeight)
        .orient("bottom");

    vis.time_yAxis = d3.svg.axis()
        .scale(vis.time_y)
        .ticks(3)
        .tickFormat(d3.format("s"))
        .orient("left");

    vis.time_area = d3.svg.area()
        .interpolate("linear")
        .x(function (d) {
            return vis.time_x(d.Year);
        })
        .y0(vis.timeSeriesHeight - vis.padding)
        .y1(function (d) {
            return vis.time_y(d.Value);
        });


    vis.timesvg.append("path")
        .datum(filteredData)
        .attr("id", "timeseriesarea")
        .attr("fill", "#A6CEE3")
        .attr("d", vis.time_area);


    vis.timesvg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + (vis.timeSeriesHeight - vis.padding) + ")")
        .call(vis.time_xAxis);

    vis.timesvg.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(" + vis.padding + ",0)")
        .call(vis.time_yAxis);

    vis.timesvg.selectAll("g.y.axis")
        .transition()
        .duration(250)
        .call(vis.time_yAxis);

    vis.timesvg.selectAll("g.x.axis")
        .transition()
        .duration(250)
        .call(vis.time_xAxis);

    vis.timesvg.selectAll("#timeseriesline").remove();
    vis.timesvg.selectAll("#timeseriesarea").datum(filteredData)
        .attr("d", vis.time_area);


}


/**
 * Initialize the map
 */
CurrentStateVisualization.prototype.initializeMap = function () {
    var vis = this;
    //Map related variables
    var width = $("#map-container").width();
    var height = 440;
    vis.mapWidth = width;
    vis.mapHeight = height;
    //Define Projection for Map
    var projection = d3.geo.mercator()
        .scale(width / 5.8)
        .translate([width / 2, 330]);

    //Define path using projection
    vis.mapPath = d3.geo.path().projection(projection);

    //Append SVG for drawing map
    vis.mapsvg = d3.select('#map-container').append('svg')
        .attr('width', width)
        .attr('height', height);


    vis.mapData = topojson.feature(vis.regionsMapData, vis.regionsMapData.objects.countries).features;

    // Define the div for the tooltip
    vis.maptip = d3.select("body").append("div")
        .attr("id", "maptip")
        .attr("class", "tooltip")
        .style("opacity", 0);

}

/**
 * Update map view
 */
CurrentStateVisualization.prototype.updateMapView = function () {

    var vis = this;
    var region = null;
    var selection = null;

    vis.filteredData = vis.filterData();
    updateLegend(vis.filteredData);

    var paths = vis.mapsvg.selectAll('path').data(vis.mapData);
    var selectedPath = null;
    //Update Stage
    paths.style('fill', function (d) {
        var regionName = null;
        if (vis.selectedRegion == "World") {
            regionName = d.properties.Region;
        } else {
            regionName = d.properties.name;
        }
        if (vis.filteredData[regionName]) {
            return vis.colorScale(vis.filteredData[regionName]);
        } else {
            //return "#68ACEC";
            return "#cccccc";
        }

    });

    //Draw using path
    paths
        .enter()
        .append('path')
        .attr('class', 'land')
        .attr('d', vis.mapPath)
        .on("mouseover", function (d, i) {
            region = d.properties.Region;
            if (vis.selectedRegion == "World") {
                region = d.properties.Region;
            } else {
                region = d.properties.name;
            }

            if (vis.filteredData[region]) {
                vis.maptip.transition()
                    .duration(200)
                    .style("opacity", .7);

                vis.maptip.html(region + "<br/><br/><b>" + vis.formatAbbreviation(vis.filteredData[region]) + "</b><br/>")
                    .style("left", (d3.event.pageX + 15) + "px")
                    .style("top", (d3.event.pageY - 80) + "px");
            }

            if (vis.selectedRegion == "World") {
                selection = paths
                    .filter(function (d) {

                        if (d.properties.Region != null) {
                            if (d.properties.Region === region) {
                                return true;
                            }
                        }
                        return false;
                    });

            } else {
                selection = d3.select(this);
            }
            selection.classed('selected-region', true);

        })
        .on("mouseout", function (d, i) {
            // d3.select(this).classed('selected-region',false);
            selection.classed('selected-region', false);
            vis.maptip.transition()
                .duration(500)
                .style("opacity", 0);
        })
        .on("click", function (d, i) {

            if (vis.selectedRegion === "World") {
                vis.addTrendLine(d.properties.Region, "region");
            } else {
                vis.addTrendLine(d.properties.name, "country");
            }

        })
        .style('fill', function (d) {
            var regionName = d.properties.Region;
            if (vis.filteredData[regionName]) {

                return vis.colorScale(vis.filteredData[regionName]);
            } else {
                //return "#68ACEC";
                return "#cccccc";
            }
        });

    vis.pathsbounded = paths;
    //Draw Boundarires
    vis.boundaries = vis.mapsvg.insert("path", ".graticule")
        .datum(topojson.mesh(vis.regionsMapData, vis.regionsMapData.objects.countries, function (a, b) {
            return a !== b;
        }))
        .attr("class", "boundary")
        .attr("d", vis.mapPath);

    //Zoom to the extent
    if ((vis.eventType === 'region') || (vis.selectedRegion !== "World"))
        vis.zoomToRegion();


    //Exit Stage
    paths.exit().remove();


    /**
     * update legend
     * @param filteredData
     */
    function updateLegend() {

        var data = vis.getDataForIndicator();


        //Find minimum and maximum for the theme

        var maximum = Math.max.apply(Math, data.map(function (o) {

            if (o["Region"] !== "All countries") {
                return parseInt(o[vis.selectedIndicator]);
            } else {
                return 0;
            }
        }));


        var minimum = Math.min.apply(Math, data.map(function (o) {
            return parseInt(o[vis.selectedIndicator]);
        }));


        //Set colorScale domain
        vis.colorScale.domain([minimum, maximum]);

        //Remove previous
        var legendTitle = vis.selectedIndicator;
        if (vis.selectedIndicator === "Infections") {
            legendTitle = "(New HIV Infections)";
        } else {
            legendTitle = "(AIDS related Deaths)";
        }

        d3.select("#legend")
            .select('ul').remove();

        d3.select("#legend").select("div").remove();

        d3.select("#legend").append("div")
            .attr("id", "legendTitle")
            .text(legendTitle);

        d3.select("#legendTitle")
            .style({
                "font-family": "sans-serif",
                "font-size": "16px",
                "font-variant": "small-caps"
            });
        //Enter
        //no data class
        var legend = d3.select("#legend")
            .append('ul')
            .attr('class', 'list-inline');

        var keys = legend.selectAll('li.key')
            .data(vis.colorScale.range());

        keys.enter().append('li')
            .attr('class', 'key')
            .style('border-top-color', String)
            .text(function (d) {
                var r = vis.colorScale.invertExtent(d);
                return vis.formatAbbreviation(r[0]);

            });

    }


    /**
     * Check if the data is numeric
     * Source: http://javascript.boxsheep.com/how-to-javascript/How-to-check-if-a-value-is-a-number/
     * @param n
     * @returns {boolean}
     */
    var isNumeric = function (n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    };

}


/**
 * Format abbreviation
 * @param x integer
 * @returns {*}
 */
CurrentStateVisualization.prototype.formatAbbreviation = function (x) {
    var vis = this;
    var formatSi = d3.format(".3s");
    var s = formatSi(x);
    switch (s[s.length - 1]) {
        case "G":
            return s.slice(0, -1) + "B";
    }
    return s;
}

/**
 * Adds a Trend Line
 * @param name The name of the region
 * @param type Visualization type
 */

CurrentStateVisualization.prototype.addTrendLine = function (name, type) {
    var vis = this;
    var data = null;
    if (type == "region") {
        data = this.timeSeriesRegionsData;
    } else {

        data = this.timeSeriesCountriesData;
    }

    data = data.filter(function (element) {

        if (element.Region === name) {
            return true;
        } else {
            return false;
        }

    });

    //Add trend line using data
    vis.trendline = d3.svg.line()
        .interpolate("linear")
        .x(function (d) {
            return vis.time_x(d.Year);
        })
        .y(function (d) {
            return vis.time_y(d.Value);
        });

    vis.timesvg.append("path")
        .datum(data)
        .transition()
        .duration(750)
        .ease('elastic')
        .attr("id", "timeseriesline")
        .attr("class", "trendline")
        .attr("d", vis.trendline);

    vis.timesvg.selectAll("#timeseriesline").datum(data)
        .transition()
        .duration(750)
        .ease('elastic')
        .attr("d", vis.trendline);

}

/**
 * Zoom to area
 * Reference: Used code from
 * https://bl.ocks.org/mbostock/4699541
 */
CurrentStateVisualization.prototype.zoomToRegion = function () {
    var vis = this;
    var regionOfInterest = vis.mapData.filter(function (d) {
        if (d.properties.Region === vis.selectedRegion) {
            return true;
        }
        return false;
    })[0];

    var bounds = vis.mapPath.bounds(regionOfInterest),
        dx = bounds[1][0] - bounds[0][0],
        dy = bounds[1][1] - bounds[0][1],
        x = (bounds[0][0] + bounds[1][0]) / 2,
        y = (bounds[0][1] + bounds[1][1]) / 2,
        scale = 2,
        translate = [(vis.mapWidth - scale * (bounds[1][0] + bounds[0][0])) / 2, (vis.mapHeight - scale * (bounds[1][1] +
        bounds[0][1])) / 2];

    vis.pathsbounded.transition()
        .duration(750)
        .style("stroke-width", 1.5 / scale + "px")
        .attr("transform", "translate(" + translate + ")scale(" + scale + ")");
    if (!vis.playing) {
        vis.boundaries.transition()
            .duration(750)
            .style("stroke-width", 1.5 / scale + "px")
            .attr("transform", "translate(" + translate + ")scale(" + scale + ")");
    } else {
        vis.boundaries
            .style("stroke-width", 1.5 / scale + "px")
            .attr("transform", "translate(" + translate + ")scale(" + scale + ")");
    }
}

/**
 * Gets the appropriate data
 */
CurrentStateVisualization.prototype.getDataForIndicator = function () {

    var vis = this;
    if ((vis.selectedIndicator == "Deaths") && (vis.selectedRegion == "World")) {
        return vis.deathsData;
    } else if ((vis.selectedIndicator == "Infections") && (vis.selectedRegion == "World")) {
        return vis.infectionsData;
    } else if ((vis.selectedIndicator == "Deaths") && (vis.selectedRegion != "World")) {
        return vis.deathsCountryData;
    } else {
        return vis.infectionsCountryData;
    }
}

/**
 * Filters the data
 */
CurrentStateVisualization.prototype.filterData = function () {
    var vis = this;
    var data = null;
    var selectionYear = "2014";
    if (vis.selectedYear != "Current") {
        selectionYear = vis.selectedYear;
    }
    data = vis.getDataForIndicator();

    var filteredData = data.filter(function (element) {

        if ((element.Year == selectionYear) && (element.Group == vis.selectedGroup)) {
            return true;
        } else {
            return false;
        }
    });

    var formattedData = {};

    filteredData.map(function (data, i) {
        var region = data.Region;
        var value = parseFloat(data[vis.selectedIndicator]);

        if ((data.Country !== undefined) && (data.Country !== null) && (data.Region == vis.selectedRegion)) {
            formattedData[data.Country] = value;
        }
        formattedData[region] = value;

    });
    //Combine Carribean and Latin America

    if (vis.selectedRegion == "World") {
        var combined = formattedData['Latin America'] + formattedData['Caribbean'];
        formattedData['Latin America and the Caribbean'] = combined;
    }

    return formattedData;

}

/**
 * Filters the data based on beginning and end dates
 * @param beginningDate
 * @param endDate
 */
CurrentStateVisualization.prototype.filterDataRange = function (beginningDate, endDate) {
    var vis = this;
    var data = vis.getDataForIndicator();

    var filteredData = data.filter(function (element) {
        var date = parseInt(element.Year);

        if ((date <= endDate && date >= beginningDate)
            && (element.Group == vis.selectedGroup)) {

            return true;
        } else {
            return false;
        }

    });

    var filteredDataRange = [];

    filteredData.map(function (data, i) {
        var region = data.Region;
        var value = parseFloat(data[vis.selectedIndicator]);
        var year = parseInt(data.Year);
        var formattedData = {};
        if ((data.Country !== undefined) && (data.Country !== null) && (data.Region == vis.selectedRegion)) {
            formattedData['Region'] = data.Country;
            formattedData['Value'] = value;
            formattedData['Year'] = vis.parseDate(year.toString());
            filteredDataRange.push(formattedData);

        } else if ((region == vis.selectedRegion) || (vis.selectedRegion == "World")) {
            formattedData['Region'] = region;
            formattedData['Value'] = value;
            formattedData['Year'] = vis.parseDate(year.toString());
            filteredDataRange.push(formattedData);
        }

        vis.timeSeriesCountriesData = filteredDataRange;

    });
    //Combine Carribean and Latin America

    if (vis.selectedRegion == "World") {

        var carribeanData = {};
        var latinAmericanData = {};
        for (var element in filteredData) {
            element = filteredData[element];
            var value = parseFloat(element[vis.selectedIndicator]);
            if (element.Region == 'Caribbean') {

                carribeanData[element.Year] = value;
            } else if (element.Region == 'Latin America') {
                latinAmericanData[element.Year] = value;
            }
        }
        for (var i = beginningDate; i <= endDate; i++) {
            var total = carribeanData[i] + latinAmericanData[i];
            if (!isNaN(total)) {
                var formattedData = {};
                formattedData['Year'] = vis.parseDate(i.toString());
                formattedData['Value'] = total;
                formattedData['Region'] = 'Latin America and the Caribbean';
                filteredDataRange.push(formattedData);
            }
        }
        vis.timeSeriesRegionsData = filteredDataRange;
    }

    var temp = vis.timeSeriesRegionsData;
    var finalFiltered = temp.filter(function (element) {

        if (vis.selectedRegion == "World") {
            if (element.Region == "All countries") {
                return true;
            } else {
                return false;
            }
        } else {

            if (element.Region == vis.selectedRegion) {
                return true;
            } else {
                return false;
            }
        }

    });

    return finalFiltered;


}


