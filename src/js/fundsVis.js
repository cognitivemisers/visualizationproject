// Stacked to Grouped bar graph for Fund Needed
// citation: https://bl.ocks.org/mbostock/3943967

/*
 * Stacked / Grouped Bar Chart - Object constructor function
 * @param _parentElement 	-- the HTML element in which to draw the Stacked/Grouped Bar Chart visualization
 * @param _data				-- funds dataset
 */

FundsBarChart = function(_parentElement, _data){

    this.parentElement = _parentElement;
    this.data = _data;

    this.displayData = []; // see data wrangling
    this.selectedType = "stacked";

    // DEBUG RAW DATA
    //console.log(this.data);

};

/*
 * Initialize DOM - include the HTML from funds.html,
 * Bind event Listeners and call initVis
 */
FundsBarChart.prototype.initDOM = function() {

    var vis = this;

    $.get("includes/funds.html", function(data) {
        $("#funds-vis").append(data);
        vis.bindEventListeners();
        vis.initVis();
    });

};

/*
 * Register Event Listeners
 */
FundsBarChart.prototype.bindEventListeners = function(){

    var vis = this;

    $('input[name="f-bartype"]').change( function() {
        vis.selectedType = $(this).val();
        vis.updateVis();
    });

};

/*
 * Initialize visualization (static content, e.g. SVG area or axes)
 */
FundsBarChart.prototype.initVis = function(){

    var vis = this;

    vis.fundsData = [];

    // Select all row & column headers
    vis.years = d3.keys(vis.data[0]).filter(function (key) { return key !== "Category"; });
    vis.fundCategory = vis.data.map(function(d) { return d.Category; });

    // Prepare the data for the stack layout
    vis.data.forEach(function (d) {
        var y0 = 0;
        vis.fundsData.push(vis.years.map(function (name, i) { return {Category: d.Category, x: name, y: (!isNaN(+d[name])) ? +d[name] : 0}; }));
    });

    // Create stack layout
    vis.stack = d3.layout.stack();

    // D3 number / currency formats
    vis.numFormat = d3.format(",");
    vis.formatCurrency = d3.format("$");

    // Create SVG drawing space
    vis.margin = {top: 40, right: 250, bottom: 20, left: 20};
    vis.width = 960 - vis.margin.left - vis.margin.right;
    vis.height = 500 - vis.margin.top - vis.margin.bottom;

    vis.svgBar = d3.select("#f-barchart").append("svg")
        .attr("width", vis.width + vis.margin.left + vis.margin.right)
        .attr("height", vis.height + vis.margin.top + vis.margin.bottom)
        .append("g")
        .attr("transform", "translate(" + vis.margin.left + "," + vis.margin.top + ")");

    // Create x, y & bar stack color scales
    vis.x = d3.scale.ordinal().rangeRoundBands([0, vis.width], .1);

    vis.y = d3.scale.linear().rangeRound([vis.height, 0]);

    vis.barColor = d3.scale.ordinal().range(colorbrewer.Paired["12"].reverse());

    vis.xAxis = d3.svg.axis()
        .scale(vis.x)
        .orient("bottom");

    vis.yAxis = d3.svg.axis()
        .scale(vis.y)
        .orient("right")
        .tickFormat(d3.format(".2s"));

    // Set the number of stack layers & groups
    vis.n = vis.data.length;
    vis.m = vis.data[0].length;

    // Create the stack layout
    vis.layers = vis.stack(vis.fundsData);

    // Calculate the Y scale based on stack vs group
    vis.yGroupMax = d3.max(vis.layers, function(layer) { return d3.max(layer, function(d) { return d.y; }); });
    vis.yStackMax = d3.max(vis.layers, function(layer) { return d3.max(layer, function(d) { return d.y0 + d.y; }); });

    // Initialize tooltip
    vis.tip = d3.tip().attr('class', 'd3-tip').html(function(d) { return d.Category + ": $" + numFormat(d.y) + "M"; });

    // Set the domain values for x, y and color
    vis.x.domain(vis.years);
    vis.y.domain([0, vis.yStackMax]);
    vis.barColor.domain(vis.fundCategory);

    // Create the stacked bars
    vis.layer = vis.svgBar.selectAll(".layer")
        .data(vis.layers)
        .enter().append("g")
        .attr("class", "layer")
        .style("fill", function(d, i) { return vis.barColor(i); });

    vis.rect = vis.layer.selectAll(".bar")
        .data(function(d) { return d; })
        .enter().append("rect")
        .attr("class", "bar")
        .attr("x", function(d) { return vis.x(d.x); })
        .attr("y", vis.height)
        .attr("width", vis.x.rangeBand())
        .attr("height", 0)
        .on('mouseover', vis.tip.show)
        .on('mouseout', vis.tip.hide);

    // Invoke tooltip
    vis.rect.call(vis.tip);

    vis.rect.transition()
        .delay(function(d, i) { return i * 10; })
        .attr("y", function(d) { return vis.y(d.y0 + d.y); })
        .attr("height", function(d) { return vis.y(d.y0) - vis.y(d.y0 + d.y); });

    // Call x axis
    vis.svgBar.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + vis.height + ")")
        .call(vis.xAxis);

    // Call y axis & set the label
    vis.svgBar.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(" + vis.width + ",0)")
        .call(vis.yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        //.attr("x", width)
        .attr("y", -11)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Funds (USD Millions)");

    // Add legend
    vis.legend = vis.svgBar.selectAll(".legend")
        .data(vis.fundCategory.reverse())
        .enter().append("g")
        .attr("class", "legend")
        .attr("transform", function(d, i) { return "translate(" + (vis.width - 620)+ "," + i * 20 + ")"; });

    vis.legend.append("rect")
        .attr("class", "legendBox")
        .attr("x", vis.width - 18)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", vis.barColor);

    vis.legend.append("text")
        .attr("class", "legendText")
        .attr("x", vis.width + 5)
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "start")
        .text(function(d) { return d; });

};

/*
 * The drawing function - should use the D3 update sequence (enter, update, exit)
 * Function parameters only needed if different kinds of updates are needed
 */
FundsBarChart.prototype.updateVis = function() {

    var vis = this;

    if (vis.selectedType === "grouped") vis.transitionGrouped();
    else vis.transitionStacked();

};

// function to transition from stacked to grouped
FundsBarChart.prototype.transitionGrouped = function() {

    var vis = this;

    vis.y.domain([0, vis.yGroupMax]);

    // Update Y axis
    vis.svgBar.select(".y.axis")
        .transition()
        .duration(1000)
        .call(vis.yAxis);

    vis.rect.transition()
        .duration(500)
        .delay(function(d, i) { return i * 10; })
        .attr("x", function(d, i, j) { return vis.x(d.x) + vis.x.rangeBand() / vis.n * j; })
        .attr("width", vis.x.rangeBand() / vis.n)
        .transition()
        .attr("y", function(d) { return vis.y(d.y); })
        .attr("height", function(d) { return vis.height - vis.y(d.y); });

};

// function to transition from grouped to stacked
FundsBarChart.prototype.transitionStacked = function() {

    var vis = this;

    vis.y.domain([0, vis.yStackMax]);

    // Update Y axis
    vis.svgBar.select(".y.axis")
        .transition()
        .duration(1000)
        .call(vis.yAxis);

    vis.rect.transition()
        .duration(500)
        .delay(function(d, i) { return i * 10; })
        .attr("y", function(d) { return vis.y(d.y0 + d.y); })
        .attr("height", function(d) { return vis.y(d.y0) - vis.y(d.y0 + d.y); })
        .transition()
        .attr("x", function(d) { return vis.x(d.x); })
        .attr("width", vis.x.rangeBand());

};
