/**
 * @file Main entry point of the Application
 * @author Sundar, Lulu and Umair
 */


//Visualizations
var historicState = [];
var currentState = null;
var futureState = [];
var fundsData = [];

// Dataset Arrays needed for different visualizations
var deathsData = [];
var futureData = [];
var currentStateData = [];

//Flag to mark contact box
var contactBoxShown = false;

var formatDate = d3.time.format("%Y");
var numFormat = d3.format(",d"),
    formatInteger = d3.format(".0f"),
    formatMillions = function (d) {
        return formatInteger(d / 1e6) + "M";
    },
    formatMega = d3.format("3.0s"),
    formatPercent = d3.format(".0%");

initialize();

/**
 * Initialize Application
 *
 */
function initialize() {

    console.log('initializing Nav Listeners');
    intializeNavListeners();
    console.log('initializing historic state visualization');
    loadHistoricalData();
    console.log('initializing current state visualization');
    loadCurrentData();
    console.log('initializing future state visualization');
    loadFutureData();
    console.log('initializing funds needed visualization');
    loadFundsData();
}

/**
 * Instantiate CurrentState Visualization
 * @param {HTML Element} element on which event is triggered
 */
function initializeCurrentStateVisualization(element) {

    currentState = new CurrentStateVisualization(element, currentStateData);
    currentState.initDOM();
}

/**
 * Instantiate HistoricState Visualization
 * @param {HTML Element} element on which event is triggered
 * @param data data needed by visualization
 */
function initializeHistoricStateVisualization(element) {
    $(document).ready(function () {
        //heatmap_display("metrics_ocmip5.json", "#heatmap", "Spectral");
        historicState = new HeatMap(element, historicState);
        historicState.initDOM();
    });
}

/**
 * Instantiate FutureState Visualization
 * @param {HTML Element} element on which event is triggered
 * @param data data needed by visualization
 */
function initializeFutureStateVisualization() {

    // Add buttons, filters, place holders for vis to the DOM
    $.get("includes/future.html", function (data) {
        $("#future-vis").append(data);

        futureState.push({
            key: "future-multi-line-vis",
            value: new AreaChart("future-multi-line-vis", futureData[0], futureData[1])
        });

        // Select different area chart data to display
        futureState[0].value.bindEventListeners();

        futureState.push({
            key: "future-difference-vis",
            value: new PeopleVis("future-difference-vis", futureData[2])
        });

        futureState.push({
            key: "future-innovative-vis",
            value: new CircleVis("future-innovative-vis", futureData[0], futureData[1])
        });

    });
}


/**
 * Instantiate Funds Visualization
 * @param {HTML Element} element on which event is triggered
 * @param data data needed by visualization
 */
function initializeFundsVisualization(element) {
    $(document).ready(function () {
        fundsNeeded = new FundsBarChart(element, fundsData);
        fundsNeeded.initDOM();
    });
}

/**
 * Binds event listeners to nav elements
 */

function intializeNavListeners() {

    $('li[id^="nav-"]').click(function () {
        scrollToElement($(this));
    });

    $('li[id="contact"]').click(function () {

        $("#contactdetails").animate({
            left: "0px",
        }, 400, function () {
            contactBoxShown = true;
        });
    });

    $('li[id="about"]').click(function () {
        $("#aboutbox").modal("toggle");
    });


    $('body').click(function () {
        if (contactBoxShown) {
            $("#contactdetails").animate({
                left: "-100px",
            }, 400, function () {
                contactBoxShown = false;
            });
        }
    });
}

/**
 * Scrolls to an element
 * @param {HTML Element} element on which event is triggered
 */
function scrollToElement(element) {
    var elementID = ($(element).attr("id"));
    var scrollTarget = "#" + elementID.substring(4);
    $('html, body').animate({
        scrollTop: $(scrollTarget).offset().top
    }, 2000);
}

/**
 * Loads Current Data
 */
function loadCurrentData() {
//Load data files in parallel
    queue()
        .defer(d3.json, "data/countries.json")
        .defer(d3.csv, "data/regional/RegionsDeaths.csv")
        .defer(d3.csv, "data/regional/deathscountry.csv")
        .defer(d3.csv, "data/regional/RegionsNewHIVInfections.csv")
        .defer(d3.csv, "data/regional/infectionscountry.csv")


        .await(function (error, regionalMap, deathsDataRegional, deathsData, infectionsDataRegional, infectionsData) {
            currentStateData.push(regionalMap);
            currentStateData.push(deathsDataRegional);
            currentStateData.push(deathsData);
            currentStateData.push(infectionsDataRegional);
            currentStateData.push(infectionsData);

            initializeCurrentStateVisualization("current-vis");
        });

}


/**
 * Loads Historical data
 */
function loadHistoricalData() {

    queue()
        .defer(d3.csv, "data/deaths.csv")
        .defer(d3.csv, "data/new_hiv_infections.csv")
        .defer(d3.csv, "data/people_living_with_hiv.csv")
        .await(function (error, deaths, new_hiv_infections, people_living_with_hiv) {
            if (!error) {


                deaths.forEach(function (d) {
                    d.Value = +d.Value;
                    d.Year = formatDate.parse(d.Year.toString());
                });

                new_hiv_infections.forEach(function (d) {
                    d.Value = +d.Value;
                    d.Year = formatDate.parse(d.Year.toString());
                });

                people_living_with_hiv.forEach(function (d) {
                    d.Value = +d.Value;
                    d.Year = formatDate.parse(d.Year.toString());
                });

                historicState.push(deaths);
                historicState.push(new_hiv_infections);
                historicState.push(people_living_with_hiv);

                initializeHistoricStateVisualization("historic-vis");

            }
        });
}

/**
 * Loads Future State Data
 */
function loadFutureData() {
    queue()
        .defer(d3.csv, "data/future_aids_deaths.csv")
        .defer(d3.csv, "data/future_new_hiv.csv")
        .await(function (error, futureDeathData, futureHivData) {
            var formatDate = d3.time.format("%Y");

            var i;
            for (i = 0; i < futureDeathData.length; i++) {
                futureDeathData[i].Year = formatDate.parse(futureDeathData[i].Year);
                futureDeathData[i].FastTrack = Number(futureDeathData[i].FastTrack);
                futureDeathData[i].NoFastTrack = Number(futureDeathData[i].NoFastTrack);
            }
            for (i = 0; i < futureHivData.length; i++) {
                futureHivData[i].Year = formatDate.parse(futureHivData[i].Year);
                futureHivData[i].FastTrack = Number(futureHivData[i].FastTrack);
                futureHivData[i].NoFastTrack = Number(futureHivData[i].NoFastTrack);
            }

            var futureDifferenceData = [];
            for (i = 0; i < futureDeathData.length; i++) {
                futureDifferenceData.push({
                    Year: futureDeathData[i].Year,
                    death: (futureDeathData[i].NoFastTrack - futureDeathData[i].FastTrack),
                    hiv: (futureHivData[i].NoFastTrack - futureHivData[i].FastTrack)
                });
            }

            futureData = [futureDeathData, futureHivData, futureDifferenceData];

            initializeFutureStateVisualization();
        });
}

/**
 * Loads Historical data
 */
function loadFundsData() {

    queue()
        .defer(d3.csv, "data/funding_needed.csv")
        .await(function (error, data) {
            if (!error) {

                fundsData = data;
                initializeFundsVisualization("funds-vis");

            }
        });
}
